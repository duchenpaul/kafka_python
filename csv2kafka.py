import pandas as pd
import json
import time

import toolkit_sqlite
import kafka_send

DB_FILE = ':memory:'
csvFile = 'sample_100000.txt'

table_name = 'acv_raw_mk_order_summary_feed'
query_sql = 'SELECT * FROM {};'.format(table_name)

print('Reading csv data...')
with toolkit_sqlite.SqliteDB(DB_FILE) as sqlitedb:
    sqlitedb.load_csv(csvFile, tableName=table_name, delimiter='|')
    df = pd.read_sql(query_sql, sqlitedb.conn)

print('Sending via kafka...')
dictList = df.to_dict(orient='records')
for i in dictList:
    json_string = json.dumps(i)
    # print(json_string)
    kafka_send.send_msg(json_string)
    time.sleep(.005)
    # break

print('Done')
