import logging_manager

import api_caller

import json
import time

from sqlalchemy import create_engine
import pandas as pd

import config
import logging

connect_info = config.connect_info

connectString = "mysql+mysqldb://{user}:{password}@{host}:{port}/{schema}?charset=utf8mb4".format(
    **connect_info)
engine = create_engine(connectString, echo=False)

query_sql = '''SELECT * FROM acv_raw_mk_order_summary_feed_test ORDER BY RAND() LIMIT 220;'''

api = api_caller.API(config.API_HOST)
tk = api.login(**config.userinfo)


def printlog(msg):
    logging_manager.log_msg('[{}] {}'.format(
        time.strftime("%Y-%m-%d %H:%M:%S"), msg), print_flg=True)


# @logging_manager.logging_to_file
def query_data(query_sql):
    '''Query data from table to dict list'''
    with engine.connect() as conn:
        with conn.begin():
            df = pd.read_sql(query_sql, con=conn)
    df['order_time'] = time.strftime("%Y-%m-%d %H:%M:%S")
    dictList = df.to_dict(orient='records')
    # print(df['MK_Member_ID'])
    return dictList


def query_data_from_csv(csvFile):
    df = pd.read_csv(csvFile)
    df['order_time'] = time.strftime("%Y-%m-%d %H:%M:%S")
    # print(df['MK_Member_ID'])
    # Replace Nana with None
    df = df.where(df.notnull(), None)
    dictList = df.to_dict(orient='records')
    return dictList


def send_to_api(dictList):
    for record in dictList:
        # print('Send data: {}'.format(list(record.values())))
        jsonData = json.dumps(record, ensure_ascii=False)
        # print(jsonData)
        api.post_data(tk, jsonData)


def take_a_break(seconds):
    print('Sleeping for {} sec...'.format(seconds))
    time.sleep(seconds)


def main():
    # printlog('Querying data...')

    # Read data from database
    # dictList = query_data(query_sql)

    # Read data from csv
    sourceFile = config.csvFile
    # printlog('Fetching data from ...'.format(sourceFile))
    dictList = query_data_from_csv(sourceFile)

    iteration = int(config.settings['iteration'])
    for i in range(0, len(dictList), iteration):
        start, end = i, i+iteration if i+iteration < len(dictList) else len(dictList)
        printlog('Send to API [{}-{}] ...'.format(start, end))
        send_to_api(dictList[start:end])
        printlog('Done')
        if i != len(dictList) - iteration:
            take_a_break(int(config.settings['pause_seconds']))

        
if __name__ == '__main__':
    main()
    take_a_break(3600)
