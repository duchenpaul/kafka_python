import requests
try:
    # Python2.7
    from urllib import urlencode
except ImportError:
    # Python3
    from urllib.parse import urlencode

import json
import logging

# url = 'nj-hdp-nb-node3:5000'
# Test
# HOST = 'localhost:5000'

# user1 = {
#     'username': 'dmanager',
#     'password': 'MerkleCDP1',
# }


class API():
    """API to post data"""
    def __init__(self, HOST):
        self.url = 'http://' + HOST

        self.sess = requests.Session()
        self.headers = {
                'Host': HOST,
                'Accept': '*/*',
                'Connection': 'Keep-Alive',
                'Content-Type': 'application/x-www-form-urlencoded',
                }

    def webpage_get(self, url, headers=dict(), allow_redirects=True):
        self.resp = self.sess.get(
            url, headers=headers, allow_redirects=allow_redirects)
        return self.resp

    def webpage_post(self, url, data, headers=dict()):
        self.req = requests.Request('POST', url, data=data, headers=headers)
        self.prepped = self.sess.prepare_request(self.req)
        self.resp = self.sess.send(self.prepped)
        return self.resp

    def login(self, username, password):
        ''''''
        self.login_url = self.url + '/login'
        self.authentication = {
            'username': username,
            'password': password,
        }
        logging.info('Login user {}'.format(username))
        # resp = self.webpage_post(self.login_url, data=urlencode(self.authentication)).text
        resp = self.webpage_post(self.login_url, data=urlencode(self.authentication), headers=self.headers).text
        try:
            token = json.loads(resp)['token']
            # with open('token', 'w') as f:
            #     f.write(token)
        except Exception as e:
            login_error_msg = 'Failed to log in, ERROR: {}, Response: {}'.format(e, resp)
            logging.error(login_error_msg)
            return None
        else:
            logging.info('User {} has logged in'.format(username))
            return token

    def logout(self, token):
        ''''''
        self.login_url = self.url + '/logout'
        self.data = {
            'token': token,
        }
        logging.info('Logout user')
        resp = self.webpage_post(self.login_url, data=urlencode(self.data), headers=self.headers).text
        logging.info(resp)

    def post_data(self, token, data):
        ''''''
        self.login_url = self.url + '/individual'
        self.data = {
            'token': token,
            'data': data,
        }
        logging.info('Post data...')
        logging.info(self.login_url)
        logging.info(str(self.data))
        resp = self.webpage_post(self.login_url, data=urlencode(self.data), headers=self.headers).text
        logging.info(resp)


if __name__ == '__main__':
    pass
