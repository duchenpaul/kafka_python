from kafka import KafkaProducer
from datetime import datetime
import time

import sys
import config

kafka_host = config.kafka_host
kafka_port = config.kafka_port
kafka_topic = config.kafka_topic


producer = KafkaProducer(bootstrap_servers=['{kafka_host}:{kafka_port}'.format(
    kafka_host=kafka_host,
    kafka_port=kafka_port
)])

def send_msg(msg):
    '''Produce message'''
    try:
        response = producer.send(kafka_topic, msg.encode('utf-8'))
    except Exception as e:
        raise
    else:
        pass
    finally:
        producer.close()

if __name__ == '__main__':
    msg = 'Pass'
    print('Send {} to topic {}'.format(msg, kafka_topic))
    send_msg(msg)
