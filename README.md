# Send data
## Compile and preparation
1. Use `compile.bat` to compile to exe
2. Go to dist folder, copy `config.ini` from script folder to `dist\send_data`
3. Put source folder to the same dir of send_data with sample data in it

File tree will be like:
```
dist 
|- send_data
    |- config.ini
|- source
    |- sample_600.csv
|- kickoff.bat
```

## Run
Run `kickoff.bat` to send data