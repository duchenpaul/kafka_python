import os
import logging

import toolkit_config
import toolkit_file

configFile = 'config.ini'
# configFile = 'config_test.ini'

logging.info('Reading config: {}'.format(configFile))

configDict = toolkit_config.read_config_general(configFile)

kafka_host = configDict['Kafka']['kafka_host']
kafka_port = configDict['Kafka']['kafka_port']
kafka_topic = configDict['Kafka']['kafka_topic']


connect_info = configDict['MySQL']


API_HOST = configDict['API']['api_host']

# Local test
# connect_info = {
#     'user': 'mysql',
#     'password': 'mysql1',
#     'host': 'localhost',
#     'port': '3307',
#     'schema': 'demo',
# }

userinfo = {
    'username': configDict['API']['username'],
    'password': configDict['API']['password'],
}


csv_source = {
    'path': configDict['SOURCE']['path'],
    'csvfile': configDict['SOURCE']['csvfile'],
}

settings = {
    'iteration': configDict['SETTINGS']['iteration'],
    'pause_seconds': configDict['SETTINGS']['pause_seconds'],
}
csvFile = toolkit_file.script_path() + os.sep + csv_source['path'] + os.sep + csv_source['csvfile']