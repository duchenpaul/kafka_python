import json
import time

from sqlalchemy import create_engine
import pandas as pd

import kafka_send
import config

connect_info = config.connect_info

connectString = "mysql+mysqldb://{user}:{password}@{host}:{port}/{schema}".format(
    **connect_info)
engine = create_engine(connectString, echo=False)

query_sql = '''SELECT * FROM acv_raw_mk_order_summary_feed LIMIT 200;'''


def printlog(msg):
    print('[{}] {}'.format(time.strftime("%Y-%m-%d %H:%M:%S"), msg))


printlog('Querying data...')
with engine.connect() as conn:
    with conn.begin():
        df = pd.read_sql(query_sql, con=conn)


printlog('Sending via kafka...')
dictList = df.to_dict(orient='records')
for i in dictList:
    json_string = json.dumps(i)
    # print(json_string)
    kafka_send.send_msg(json_string)
    time.sleep(.005)
