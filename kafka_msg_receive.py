#!/usr/bin/env python

from kafka import KafkaConsumer
from datetime import datetime
import time

import config

kafka_host = config.kafka_host
kafka_port = config.kafka_port
kafka_topic = config.kafka_topic

consumer = KafkaConsumer(
    kafka_topic,
    bootstrap_servers=['{kafka_host}:{kafka_port}'.format(kafka_host=kafka_host, kafka_port=kafka_port)]
)

print('Listening to ' + kafka_topic)

for message in consumer:
    # content = json.loads(message.value)
    print(message.value)
